��            )         �     �     �     �     �  	   �     �               %     -     ;     O  
   g     r     ~     �  	   �     �     �  *   �     �                    #     4     E     b     w     �  `  �  "   �  )   "  *   L     w     �     �  (   �  (   �     �                6     R     c  /   u     �     �     �     �  *   �          2     E  
   R     ]  
   n  (   y  
   �  	   �     �                     	                                                                                    
                               &Mark Current Line A LilyPond Music Editor About About {appname} All Files Back Clear &All Marks Clear &Error Marks Credits DocBook Files Go to next position Go to previous position HTML Files LaTeX Files Licensed under the {gpl}. LilyPond Files Next Mark Previous Mark Scheme Files Send an e-mail message to the maintainers. Texinfo Files Version file menu title&Edit menu title&File menu title&View submenu title&Import/Export submenu title&Print submenu titleClose submenu titleSave Project-Id-Version: frescobaldi 3.2
Report-Msgid-Bugs-To: info@frescobaldi.org
PO-Revision-Date: 2021-09-26 12:23+0900
Last-Translator: Jun Tamura <j.tamura@me.com>
Language-Team: Japanese
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Poedit 3.0
 現在の行をマークする(&M) Lilypondミュージックエディター このアプリケーションについて {appname}について 全てのファイル 戻る 全てのマークをクリアする(&A) エラーマークをクリアする(&E) クレジット DocBookファイル 次の位置に移動 一つ前の位置に移動 HTMLファイル LaTexファイル {gpl}の元にライセンスされています Lilypondファイル 次のマーク 一つ前のマーク Schemeファイル メンテナーに電子メールを送信 Texinfoファイル ヴァージョン ファイル 編集(&E) ファイル(&F) 表示(&V) インポート／エクスポート(&I) 印刷(&P) 閉じる 保存 