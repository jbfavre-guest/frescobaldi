#!/bin/sh

cat <<EOF
The script $0 is obsolete as of December 2014.
Please use "debian/rules get-orig-source" instead.
See debian/README.source for more information.

EOF
exit 0

rm -fr frescobaldi_app/icons/Tango
find frescobaldi_app/hyphdicts -name '*.txt*' -exec rm -f {} \;
find frescobaldi_app/hyphdicts -name '*.dic' -exec rm -f {} \;
